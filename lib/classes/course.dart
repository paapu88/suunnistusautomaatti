// a single orienteering course
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:flutter/services.dart';
import 'package:flutter_beep/flutter_beep.dart';
import 'package:vibration/vibration.dart';

class Course {
  String courseName;
  String runnerName;
  List<CircleMarker> controls;

  var punchTimes = <DateTime>[];
  int currentControl = 0;
  Distance distance = new Distance();

  Course({this.courseName, this.runnerName, this.controls});

  bool check_punch({@required LatLng latLon, double minDist = 20.0}) {
    // check if we are punching
    print("controls: " + this.controls.length.toString());
    if (currentControl >= this.controls.length) {
      return true;
    }
    var latLonControl = this.controls[currentControl].point;
    double d = distance(latLon, latLonControl);
    print("Distance to next control:" + d.toString());
    if (d < minDist) {
      Vibration.vibrate();
      FlutterBeep.playSysSound(AndroidSoundIDs.TONE_CDMA_ABBR_ALERT);
      currentControl += 1;
      punchTimes.add(DateTime.now());
      return true;
    }
    return false;
  }

  bool check_finish() {
    // check if we have punched start, all controls and finish
    if (currentControl >= this.controls.length) {
      return true;
    }
    return false;
  }
}
