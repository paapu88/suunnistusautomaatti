import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:http/http.dart' as http;
import 'package:loyda_suomi/pages/home.dart';
import 'package:loyda_suomi/pages/map_ant.dart';
import 'dart:convert';

import '../widgets/drawer.dart';

class Courses {
  final String tracks;
  var courses = new List<String>();

  Courses({this.tracks}) {
    var parts = new List<String>();

    parts = this.tracks.split('{');
    parts = parts.sublist(1, parts.length);
    print("parts------------- ${parts}");
    //get iterator to the list
    var myListIter = parts.iterator;
    //iterate over the list
    while (myListIter.moveNext()) {
      var nameParts = new List<String>();
      String name = myListIter.current.toString();
      name = name.replaceFirst(RegExp(','), ':');
      name = name.replaceFirst(RegExp('}'), '');
      nameParts = name.split(':');
      courses.add(nameParts[3] + nameParts[1]);
    }
    courses = courses..sort((b, a) => a.compareTo(b));
    print("COURSES:");
    print(courses);
  }

  factory Courses.fromJson(Map<String, dynamic> json) {
    print(json['tracks']);
    return Courses(tracks: json['tracks'].join());
  }
}

Future<Courses> fetchCourses() async {
  final response = await http.get('https://map-and-db.herokuapp.com/tracks');
  //print(url + name);
  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    print("RESPONSE FINE A !!");
    print(json.decode(response.body));

    return Courses.fromJson(json.decode(response.body));
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    print("course RESPONSE BAD !!");
    print(response.statusCode.toString());
    throw Exception('Failed to load Rastit' + response.statusCode.toString());
  }
}

class CourseSelection extends StatelessWidget {
  static const String route = 'CourseSelection';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Select a course for orienteering!"),
      ),
      body: Padding(
        padding: EdgeInsets.all(8.0),
        child: FutureBuilder<Courses>(
          future: fetchCourses(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Column(
                children: <Widget>[
                  Expanded(
                    child: ListView.builder(
                      itemCount: snapshot.data.courses.length,
                      itemBuilder: (BuildContext context, int index) {
                        var post = snapshot.data.courses[index];

                        return Container(
                            child: Card(
                          child: ListTile(
                            title: Text(post),
                            onTap: () => onTapped(post, context),
                          ),
                        ));
                      },
                    ),
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }

            // By default, show a loading spinner.
            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }

  void onTapped(String post, BuildContext context) {
    // navigate to the next screen.
    var parts = new List<String>();
    parts = post.split(" ");
    print("NEXT! ${parts[2]}");
    Navigator.pushReplacementNamed(context, CustomCrsPage.route,
        arguments: parts[2]);
  }
}
