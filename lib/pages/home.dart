import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import '../pages/map_ant.dart';
import '../pages/course_selection.dart';

import '../widgets/drawer.dart';

class HomePage extends StatelessWidget {
  static const String route = '/';
  final titles = ['Run a course', 'Check results'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Select")),
      body: ListView.builder(
        itemCount: titles.length,
        itemBuilder: (BuildContext context, int index) {
          var post = titles[index];

          return Container(
              child: Card(
            child: ListTile(
              title: Text(post),
              onTap: () => onTapped(post, context),
            ),
          ));
        },
      ),
    );
  }

  void onTapped(String post, BuildContext context) {
    // navigate to the next screen.
    if (post == titles[0]) {
      Navigator.pushReplacementNamed(context, CourseSelection.route);
    } else {
      print("not implemented...");
    }
  }
}
