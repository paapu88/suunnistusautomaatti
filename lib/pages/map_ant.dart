import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:latlong/latlong.dart';
import 'package:proj4dart/proj4dart.dart' as proj4;
import 'package:geolocator/geolocator.dart';
import 'dart:async';
import 'package:map_controller/map_controller.dart';
import '../classes/course.dart';
import '../classes/controls.dart';

class CustomCrsPage extends StatefulWidget {
  final String courseName;
  final String userName;
  CustomCrsPage({this.courseName = null, this.userName = null});
  static const String route = 'custom_crs';

  @override
  _CustomCrsPageState createState() => _CustomCrsPageState();
}

class _CustomCrsPageState extends State<CustomCrsPage> {
  Geolocator _geolocator;
  Position _position;
  Piste piste;
  bool found = false;
  double latitude;
  double longitude;
  Proj4Crs epsg3067CRS;
  double maxZoom;
  var rastit;
  var circles = <CircleMarker>[];
  proj4.Projection epsg3067;
  // Define start center
  //proj4.Point point = proj4.Point(x: 60.192059, y: 24.945831);
  String initText = 'Map centered to';
  MapController mapController;
  StatefulMapController statefulMapController;
  StreamSubscription<StatefulMapControllerStateChange> sub;

  bool ready = false;
  bool doCenter = true;
  StreamSubscription<Position> positionStream;
  var geolocator = Geolocator();
  var course;
  var locationOptions =
      LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 10);

  Future<List<CircleMarker>> getRastit({String courseName}) async {
    var r;
    if (rastit != null) {
      //print("radtitOld: " + rastit.toString());
      return rastit;
    }
    r = await fetchRastit(courseName: courseName);
    rastit = r.controls;
    return rastit;
  }

  Future<void> updateLocation() async {
    //try {
    Position newPosition = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .timeout(new Duration(seconds: 5));

    setState(() {
      _position = newPosition;
      print("POSITION:" + _position.toString());
    });
    //} catch (e) {
    //  print('Error: ${e.toString()}');
    //}
  }

  @override
  void initState() {
    mapController = MapController();
    statefulMapController = StatefulMapController(mapController: mapController);
    statefulMapController.onReady.then((_) => setState(() => ready = true));
    sub = statefulMapController.changeFeed.listen((change) => setState(() {}));
    super.initState();
    _geolocator = Geolocator();
    epsg3067 = proj4.Projection('EPSG:3067') ??
        proj4.Projection.add('EPSG:3067',
            '+proj=utm +zone=35 +ellps=GRS80 +units=m +towgs84=0,0,0,-0,-0,-0,0 +no_defs');
    // mapAnt example zoom level resolutions
    final resolutions = <double>[
      8192,
      4096,
      2048,
      1024,
      512,
      256,
      128,
      64,
      32,
      16,
      8,
      4,
      2,
      1,
      0.5,
      0.25
    ];

    final epsg3067Bounds = Bounds<double>(
      CustomPoint<double>(-548576.0, 6291456.0),
      CustomPoint<double>(1548576.0, 8388608.000000),
    );

    maxZoom = (resolutions.length - 1).toDouble();
    // Define CRS
    epsg3067CRS = Proj4Crs.fromFactory(
      // CRS code
      code: 'EPSG:3067',
      // your proj4 delegate
      proj4Projection: epsg3067,
      // Resolution factors (projection units per pixel, for example meters/pixel)
      // for zoom levels; specify either scales or resolutions, not both
      resolutions: resolutions,
      // Bounds of the CRS, in projected coordinates
      // (if not specified, the layer's which uses this CRS will be infinite)
      bounds: epsg3067Bounds,
      // Tile origin, in projected coordinates, if set, this overrides the transformation option
      // Some goeserver changes origin based on zoom level
      // and some are not at all (use explicit/implicit null or use [CustomPoint(0, 0)])
      // @see https://github.com/kartena/Proj4Leaflet/pull/171
      origins: [CustomPoint(0, 0)],
      // Scale factors (pixels per projection unit, for example pixels/meter) for zoom levels;
      // specify either scales or resolutions, not both
      scales: null,
      // The transformation to use when transforming projected coordinates into pixel coordinates
      transformation: null,
    );
  }

  LatLng get_point() {
    if (ready && doCenter) {
      //print(mapController.zoom);
      statefulMapController
          .centerOnPoint(LatLng(_position.latitude, _position.longitude));
    }
    return LatLng(_position.latitude, _position.longitude);
  }

  @override
  Widget build(BuildContext context) {
    final String args = ModalRoute.of(context).settings.arguments;
    var circles;
    return Scaffold(
      //appBar: AppBar(title: Text('Custom CRS')),
      //drawer: buildDrawer(context, CustomCrsPage.route),
      body: Padding(
        padding: EdgeInsets.all(8.0),
        child: FutureBuilder(
          future: Future.wait([getRastit(courseName: args), updateLocation()]),
          builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
            print("snapshot:" + snapshot.toString());

            if (!snapshot.hasData) {
              return Text('No data!');
            }
            if (_position == null) {
              return Text('No POSITION data!');
            }
            piste = Piste(lat: _position.latitude, lon: _position.longitude);
            circles = rastit + piste.piste;
            //circles = piste.piste;

            if (course == null) {
              course = Course(
                  runnerName: widget.userName,
                  courseName: widget.courseName,
                  controls: rastit);
            }

            course.check_punch(latLon: piste.piste[0].point);

            return FlutterMap(
              mapController: mapController,
              options: MapOptions(
                  // Set the default CRS
                  crs: epsg3067CRS,
                  center: get_point(),
                  //center: userCenter,
                  zoom: 12.0,
                  // Set maxZoom usually scales.length - 1 OR resolutions.length - 1
                  // but not greater
                  maxZoom: maxZoom,
                  onTap: (point) {
                    doCenter = !doCenter;
                    get_point();
                  }),
              layers: [
                TileLayerOptions(
                  wmsOptions: WMSTileLayerOptions(
                    // Set the WMS layer's CRS too
                    crs: epsg3067CRS,
                    baseUrl: 'http://wms.mapant.fi/wms.php?',
                    //layers: ['default'],
                    //baseUrl: 'http://avaa.tdata.fi/geoserver/osm_finland/wms?',
                    //layers: ['osm_finland:osm-finland'],
                  ),
                ),
                //if (userLocation != null) {
                CircleLayerOptions(
                    //circles: snapshot.data.controls + piste.piste
                    circles: circles),
                //}
              ],
            );
          },
        ),
      ),
    );
  }
}
