import 'package:flutter/material.dart';

import '../pages/map_ant.dart';
import '../pages/course_selection.dart';

Drawer buildDrawer(BuildContext context, String currentRoute) {
  return Drawer(
    child: ListView(
      children: <Widget>[
        const DrawerHeader(
          child: Center(
            child: Text('Select:'),
          ),
        ),
        ListTile(
          title: const Text('Choose a course for running'),
          selected: currentRoute == CourseSelection.route,
          onTap: () {
            Navigator.pushReplacementNamed(context, CourseSelection.route);
          },
        ),
      ],
    ),
  );
}
